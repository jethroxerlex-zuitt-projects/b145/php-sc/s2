<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S2:Activity for Loops</title>
</head>
<body>

	<h1>Divisible by 5</h1>
	<?php divisible(); ?>

	<h1>Array Manipulation</h1>

	<?php array_push($students, 'John Smith',); ?>
	<pre><?php print_r($students); ?></pre>
	<pre><?php print_r(array_count_values($students)); ?></pre>

	<?php array_unshift($students, 'Jane Smith'); ?>
	<?php array_shift($students,); ?>

	<?php print_r(array_count_values($students)); ?>

	
</body>
</html>